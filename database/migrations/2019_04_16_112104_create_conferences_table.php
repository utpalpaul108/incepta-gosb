<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conferences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('theme')->nullable();
            $table->string('country')->nullable();
            $table->unsignedBigInteger('conference_type_id');
            $table->foreign('conference_type_id')->references('id')->on('conference_types')->onDelete('cascade');
            $table->string('conference_date')->nullable();
            $table->string('time_session')->nullable();
            $table->text('details')->nullable();
            $table->text('extra_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conferences');
    }
}
