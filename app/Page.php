<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable=['page_title', 'template_id', 'contents', 'slug'];
    protected $casts=['contents'=>'array'];

    public function template(){
        return $this->belongsTo('App\PageTemplate');
    }
}
