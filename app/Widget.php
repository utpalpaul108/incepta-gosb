<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $fillable=['name','contents'];

    protected $casts=['contents'=>'array'];
}
