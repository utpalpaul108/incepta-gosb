<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table='user_profile';
    protected $fillable=['user_id','about','education','experience','awards','address','website','social_links'];
    protected $casts=['social_links'=>'array'];
}
