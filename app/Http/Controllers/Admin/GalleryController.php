<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\GalleryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries=Gallery::all();
        return view('admin.gallery.index',compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=GalleryCategory::all();
        return view('admin.gallery.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData=$request->all();
        if ($request->hasFile('image')){
            $path=$request->file('image')->store('images');
            $image = Image::make(Storage::get($path))->fit(750, 519)->encode();
            Storage::put($path, $image);
            $allData['image']=$path;
        }
        Gallery::create($allData);
        flash('Gallery Element Created Successfully');
        return redirect()->action('Admin\GalleryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=GalleryCategory::all();
        $gallery=Gallery::find($id);
        return view('admin.gallery.edit',compact('categories','gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery=Gallery::find($id);
        $gallery->title=$request->title;
        $gallery->category_id=$request->category_id;
        if ($request->hasFile('image')){
            Storage::delete($gallery->image);
            $path=$request->file('image')->store('images');
            $image = Image::make(Storage::get($path))->fit(750, 519)->encode();
            Storage::put($path, $image);
            $gallery->image=$path;
        }
        $gallery->save();
        flash('Gallery element updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery=Gallery::find($id);
        Storage::delete($gallery->image);
        Gallery::destroy($id);
        flash('Gallery element deleted successfully');
        return redirect()->action('Admin\GalleryController@index');
    }
}
