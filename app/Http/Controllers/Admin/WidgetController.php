<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Forum;
use App\Widget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $widgets=Widget::all();
        return view('admin.widget.index',compact('widgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.widget.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Widget::create($request->all());
        flash('New Widget created successfully');
        return redirect()->action('Admin\WidgetController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($name)
    {
        $widget=Widget::where('name',$name)->first();
//        dd($widget->contents);
        return view('admin.widget.'.$name, compact('widget'));
    }

    public function update(Request $request, $name)
    {
        Widget::updateOrCreate(['name'=>$name],['contents'=>$request->contents]);
        flash('Widget updated successfully');
        return redirect()->back();
    }

}
