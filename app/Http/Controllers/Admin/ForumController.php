<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Forum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums=Forum::all();
        return view('admin.forum.index',compact('forums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('admin.forum.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData=$request->all();
        $allData['views']=0;
        $allData['created_by']=Auth::id();
        Forum::create($allData);
        flash('Forum created successfully');
        return redirect()->action('Admin\ForumController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Category::all();
        $forum=Forum::find($id);
        return view('admin.forum.edit',compact('forum','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $forum=Forum::find($id);
        $forum->title=$request->title;
        $forum->details=$request->details;
        $forum->category_id=$request->category_id;
        $forum->updated_by=Auth::id();
        $forum->save();
        flash('Forum updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Forum::destroy($id);
        flash('Forum deleted successfully');
        return redirect()->action('Admin\ForumController@index');
    }
}
