<?php

namespace App\Http\Controllers\Admin;

use App\SiteSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SiteSettingController extends Controller
{
    public function index(){
        return view('admin.site_setting.index');
    }

    public function update(Request $request){
        $allData=$request->except('_token','logo');
        foreach ($allData as $key=>$data){
            if ($key=='social_icons'){
                $data=json_encode($data);
            }
            SiteSetting::updateOrCreate(['key'=>$key],['value'=>$data]);
        }

        if ($request->hasFile('logo')){
            $path=$request->file('logo')->store('images');
            $image = Image::make(Storage::get($path))->fit(161, 95)->encode();
            Storage::put($path, $image);
            SiteSetting::updateOrCreate(['key'=>'logo'],['value'=>$path]);
        }

        flash('Site Settings Updated Successfully');
        return redirect()->back();
    }
}
