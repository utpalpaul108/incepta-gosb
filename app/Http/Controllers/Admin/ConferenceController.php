<?php

namespace App\Http\Controllers\Admin;

use App\Conference;
use App\ConferenceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conferences=Conference::all();
        return view('admin.conference.index',compact('conferences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $conference_types = ConferenceType::all();
        return view('admin.conference.create',compact('conference_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Conference::create($request->all());
        flash('Conference created successfully');
        return redirect()->action('Admin\ConferenceController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $conference_types = ConferenceType::all();
        $conference=Conference::find($id);
        return view('admin.conference.edit',compact('conference','conference_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $conference=Conference::find($id);
        $conference->title=$request->title;
        $conference->theme=$request->theme;
        $conference->country=$request->country;
        $conference->details=$request->details;
        $conference->conference_type_id=$request->conference_type_id;
        $conference->conference_day=$request->conference_day;
        $conference->time_session=$request->time_session;
        $conference->extra_data=$request->extra_data;
        $conference->save();
        flash('Conference updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Conference::destroy($id);
        flash('Conference deleted successfully');
        return redirect()->action('Admin\ConferenceController@index');
    }
}
