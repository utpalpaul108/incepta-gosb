<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConferenceType extends Model
{
    protected $fillable=['name'];
}
