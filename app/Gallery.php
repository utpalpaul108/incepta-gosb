<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable=['title','category_id','image'];

    public function category(){
        return $this->belongsTo('App\GalleryCategory','category_id');
    }
}
