<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable=['title','category_id','details','feature_image','created_by','updated_by'];

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

    public function post_created(){
        return $this->belongsTo('App\User','created_by');
    }
}
