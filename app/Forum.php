<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $fillable=['title','details','category_id','created_by','views'];

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

    public function creator(){
        return $this->belongsTo('App\User','created_by');
    }
}
