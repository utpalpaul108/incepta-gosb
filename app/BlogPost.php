<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogPost extends Model
{
    use SoftDeletes;
    protected $fillable=['title','category_id','feature_image','details','created_by'];

    public function post_created(){
        return $this->belongsTo('App\User','created_by');
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }
}
