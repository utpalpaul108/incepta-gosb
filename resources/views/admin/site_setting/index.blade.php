@extends('admin.layout.app')

@section('page_title','Admin | Edit site settings')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\SiteSettingController@index') }}">Site Settings</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Site Settings</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">
                @include('flash::message')
                <form id="sliderGroup" action="{{ action('Admin\SiteSettingController@update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Site Settings </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Site Settings
                                            </legend>
                                            <div class="form-group">
                                                <label>Phone No</label>
                                                <input type="text" class="form-control" name="phone_no" value="{{ setting('phone_no') }}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" name="email" value="{{ setting('email') }}"/>
                                            </div>

                                            <div class="form-group row" id="socialIconGroup">
                                                <label class="col-md-12">Social Icons</label>

                                                @if(!is_null(setting('social_icons')))
                                                    @foreach(json_decode(setting('social_icons'),true) as $index=>$social_icon)
                                                        <div class="col-md-12 eachSocialIcon mt-3">
                                                            <div class="input-group input-group-md">
                                                                <input type="text" class="form-control" name="social_icons[{{ $index }}][name]" placeholder="Name" value="{{ $social_icon['name'] }}" required >
                                                                <input type="text" class="form-control" name="social_icons[{{ $index }}][icon]" placeholder="Icon" value="{{ $social_icon['icon'] }}" required >
                                                                <input type="text" class="form-control" name="social_icons[{{ $index }}][url]" placeholder="URL" value="{{ $social_icon['url'] }}" required >
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-default" type="button" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().parent().remove()}"><i class="fa fa-minus"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                @else
                                                    <div class="col-md-12 eachSocialIcon mt-3">
                                                        <div class="input-group input-group-md">
                                                            <input type="text" class="form-control" name="social_icons[0][name]" placeholder="Name" required >
                                                            <input type="text" class="form-control" name="social_icons[0][icon]" placeholder="Icon" required >
                                                            <input type="text" class="form-control" name="social_icons[0][url]" placeholder="URL" required >
                                                            <div class="input-group-append">
                                                                <button class="btn btn-default" type="button" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().parent().remove()}"><i class="fa fa-minus"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary pull-right" type="button" id="addSocialIcon">Add <i class="fa fa-plus-circle"></i></button>
                                            </div>

                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-tags"></i> Logo:</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-body text-center">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                    <img src="@if(!is_null(setting('logo'))){{ '/storage/' .setting('logo') }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="slider image">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                        <input type="file" name="logo">
                                                    </span>
                                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            var socialElements= $('.eachSocialIcon').length;
            $('#addSocialIcon').click(function () {
                var data ='<div class="col-md-12 eachSocialIcon mt-3">\n' +
                    '                                                    <div class="input-group input-group-md">\n' +
                    '                                                        <input type="text" class="form-control" name="social_icons['+socialElements+'][name]" placeholder="Name" required>\n' +
                    '                                                        <input type="text" class="form-control" name="social_icons['+socialElements+'][icon]" placeholder="Icon" required>\n' +
                    '                                                        <input type="text" class="form-control" name="social_icons['+socialElements+'][url]" placeholder="URL" required>\n' +
                    '                                                        <div class="input-group-append">\n' +
                    '                                                            <button class="btn btn-default" type="button" onclick="if(confirm(\'Are You Sure ?\')){$(this).parent().parent().parent().remove()}"><i class="fa fa-minus"></i></button>\n' +
                    '                                                        </div>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>';
                $('#socialIconGroup').append(data);

                socialElements++;
            });


            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    // title : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider title is required'
                    //         },
                    //     }
                    // },
                    // subtitle : {
                    //     validators : {
                    //         notEmpty : {
                    //             message : 'Slider subtitle is required'
                    //         }
                    //     }
                    // }
                }
            });

            // end profile form

        })
    </script>
    {{-- For Flash message--}}
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
