@extends('admin.layout.app')

@section('page_title','Admin | Dashboard')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                {{--<h1 class="page-header"><i class="fa fa-table fa-fw "></i> Dashboard</h1>--}}
            </div>
        </div>
        <div>
            <div>

                <!-- widget grid -->
                <section id="widget-grid" class="">


                </section>
                <!-- end widget grid -->

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
