@extends('admin.layout.app')

@section('page_title','Admin | Menu')
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
{{--        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>--}}
{{--        <li class="breadcrumb-item"><a href="{{ action('Admin\MenuController@index') }}">Menu Builder</a></li>--}}
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-desktop"></i> Dashboard <span>> Menu Builder</span></h1>
            </div>
        </div>
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget well" id="wid-id-0">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <div class="widget-header">
                                <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                <h2>My Data </h2>
                            </div>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <div id="nestable-menu">
                                    <button type="button" class="btn btn-default" data-action="expand-all">
                                        Expand All
                                    </button>
                                    <button type="button" class="btn btn-default" data-action="collapse-all">
                                        Collapse All
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <h6>Manage Menu <span class="add-menu-items"><i class="fa fa-plus-circle"></i></span></h6>

                                        <div class="dd" id="nestable">
                                            <ol class="dd-list">
                                                @foreach($items as $item)
                                                    <li class="dd-item" data-id="{{ $item->id }}">
                                                        <div class="pull-right" style="padding: 8px 0px;">
                                                            <div class="btn btn-xs btn-danger pull-right button-delete" style="margin-left: 5px ; margin-right: 5px" data-id="{{ $item->id }}">
                                                                <i class="fa fa-trash"></i> Delete
                                                            </div>
                                                            <div class="btn btn-xs btn-primary pull-right button-edit" style="margin-right: 5px" data-id="{{ $item->id }}" data-title="{{ $item->title }}" data-target="{{ $item->target }}" data-url="{{ $item->url }}">
                                                                <i class="fa fa-edit"></i> Edit
                                                            </div>
                                                        </div>
                                                        <div class="dd-handle">
                                                            {{ $item->title }}
                                                        </div>
                                                        @if(!$item->children->isEmpty())
                                                            @include('admin.menu.menu-items', ['items' => $item->children])
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="widget-body">
                                            <form class="add-menu form-horizontal">
                                                <fieldset>
                                                    <legend>Add Menu Item</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Title</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control new-menu-item" name="title" placeholder="Menu Title" type="text" id="title" required>
                                                            <p class="note menu-title-error" style="display: none; color: red"><strong>Note:</strong> Menu Title Required</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="url">Page</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control new-menu-item" name="url" id="url" required>
                                                                <option value="">No Link</option>
                                                                @foreach($pages as $page)
                                                                    <option value="{{ $page->slug }}">{{ $page->page_title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="parent">Parent</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control new-menu-item" name="parent_id" id="parent" required>
                                                                <option value="">None</option>
                                                                @foreach($menus as $menu)
                                                                    <option value="{{ $menu->id }}">{{ $menu->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="target">Page Target</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control new-menu-item" id="target" required>
                                                                <option value="_self">Current Tab</option>
                                                                <option value="_blank">New Tab</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <button class="btn btn-primary btn-sm pull-right" type="submit" id="addMenu">
                                                                Create
                                                            </button>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                            </form>

                                            <form class="form-horizontal edit-menu" style="display: none">
                                                <fieldset>
                                                    <legend>Edit Menu Item</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Title</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control" name="title" placeholder="Menu Title" type="text" id="updateTitle" required>
                                                            <p class="note update-menu-title-error" style="display: none; color: red"><strong>Note:</strong> Menu Title Required</p>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="updateId" name="id">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="updateUrl">Page</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control" name="url" id="updateUrl" required>
                                                                <option value="#">No Link</option>
                                                                @foreach($pages as $page)
                                                                    <option value="{{ $page->slug }}">{{ $page->page_title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="updatePageTarget">Page Target</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control" id="updatePageTarget" required>
                                                                <option value="_self">Current Tab</option>
                                                                <option value="_blank">New Tab</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <button class="btn btn-primary btn-sm pull-right" type="submit" id="updateMenu">
                                                                Update
                                                            </button>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- WIDGET END -->

            </div>

            <!-- end row -->

        </section>
    </div>
@endsection

@section('script')
    {{-- Jquery Toster--}}
    <script src="/ic_admin/js/jquery.toaster.js"></script>
    <script>
        $(document).ready(function() {
            // PAGE RELATED SCRIPTS

            // activate Nestable for list 1
            $('#nestable').nestable({
                group : 1
            });

            $('.dd').on('change', function (e) {
                $.post('/admin/menu/order', {
                    order: JSON.stringify($('.dd').nestable('serialize')),
                    _token: '{{ csrf_token() }}'
                }, function (data) {
                    $.toaster('Menu Order Updated Successfully', 'Success', 'success');
                });
            });


            $('#nestable-menu').on('click', function(e) {
                var target = $(e.target), action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            $('#addMenu').click(function (event) {
                event.preventDefault();
                var title=$('#title').val();
                var url=$('#url').val();
                var parent_id=$('#parent').val();
                var target=$('#target').val();

                if (title == ''){
                    $('.menu-title-error').show();
                    return false;
                }
                else {
                    $('.menu-title-error').hide();
                }

                $.post('/admin/menu/create', {
                    title: title,
                    url: url,
                    target: target,
                    parent_id: parent_id,
                    _token: '{{ csrf_token() }}'
                },function (data) {
                    $('#nestable').load('/admin/menu/load_data');
                    $('.new-menu-item').val('');
                    $.toaster('Menu Updated Successfully', 'Success', 'success');
                });
            });

            $(document).on('click', '#updateMenu', function (event) {
                event.preventDefault();
                var id=$('#updateId').val();
                var title=$('#updateTitle').val();
                var url=$('#updateUrl').val();
                var target=$('#updatePageTarget').val();
                if (title == ''){
                    $('.update-menu-title-error').show();
                    return false;
                }
                else {
                    $('.menu-title-error').hide();
                }

                $.post('/admin/menu/update', {
                    id: id,
                    title: title,
                    url: url,
                    target: target,
                    _token: '{{ csrf_token() }}'
                },function (data) {
                    $('#nestable').load('/admin/menu/load_data');
                    $.toaster('Menu Updated Successfully', 'Success', 'success');
                });
            });

            $(document).on('click','.button-delete', function () {
                if (confirm('Are You Sure ?')) {
                    var targetId = $(this).data('id');
                    $.get('/admin/menu/delete/'+targetId,function (data) {
                        $('#nestable').load('/admin/menu/load_data');
                    });

                    $.toaster('Menu Deleated Successfully', 'Success', 'success');
                }
            });

            $(document).on('click', '.add-menu-items', function () {
                $('.add-menu').show();
                $('.edit-menu').hide();
            });
            $(document).on('click','.button-edit', function () {
                var targetId = $(this).data('id');
                var targetTitle = $(this).data('title');
                var targetUrl = $(this).data('url');
                var pageTarget = $(this).data('target');
                $('#updateId').val(targetId);
                $('#updateTitle').val(targetTitle);
                $('#updateUrl').val(targetUrl);
                $('#updatePageTarget').val(pageTarget);

                $('.add-menu').hide();
                $('.edit-menu').show();
            });
        });

        $('.note').delay(3000).fadeOut(350);
    </script>
@endsection
