<div class="widget-body">
    <fieldset>
        <legend>
            Form Elements
        </legend>

        <div class="form-group">
            <label>Page Contents</label>
            <textarea class="form-control editor" id="editor" rows="8" name="contents[page_content]" required>{{ $page->contents['page_content'] ?? '' }}</textarea>
        </div>
    </fieldset>

    @include('admin.template.partials.form_submit')
</div>



