
<div class="widget-body">
    <fieldset>
        <legend>
            Contact Us Page
        </legend>
        <div class="form-group">
            <label>Contact Title</label>
            <input type="text" class="form-control" name="contents[slider_title]" value="{{ $page->contents['slider_title'] ?? '' }}" required/>
        </div>
        <div class="form-group">
            <label>Contact Subtitle</label>
            <textarea class="form-control editor" name="contents[slider_subtitle]" rows="8">{!! $page->contents['slider_subtitle'] ?? '' !!}</textarea>
        </div>
        <div class="form-group">
            <label>Slider Image</label>
            <div class="box-body text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                        <img src="@if(isset($page->contents['slider_image'])){{ '/storage/' .$page->contents['slider_image'] }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="slider image">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                    <div>
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="slider_image" @if(!isset($page->contents['slider_image'])){{ 'required' }} @endif>
                    </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

</div>

<div class="widget-body">
    <fieldset>
        <legend>
            Contact Info elements
        </legend>
    </fieldset>

    <span id="contact-info-group">
        @if(isset($page->contents['contact_info']))
            @foreach($page->contents['contact_info'] as $index=>$contact_info)
                <fieldset class="single-contact-info">
                    <div class="form-group">
                        <label>Contact Title</label>
                        <input type="text" class="form-control" name="contents[contact_info][{{ $index }}][title]" value="@if(isset($contact_info['title'])){{ $contact_info['title'] }} @endif" required/>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control editor" name="contents[contact_info][{{ $index }}][address]" rows="8">@if(isset($contact_info['address'])){{ $contact_info['address'] }} @endif</textarea>
                    </div>
                    <div class="form-group">
                        <label>Phone Numbers</label>
                        <input type="text" class="form-control" name="contents[contact_info][{{ $index }}][phone_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" value="@if(isset($contact_info['phone_no'])){{ $contact_info['phone_no'] }} @endif" required/>
                    </div>
                    <div class="form-group">
                        <label>Mobile Numbers</label>
                        <input type="text" class="form-control" name="contents[contact_info][{{ $index }}][mobile_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" value="@if(isset($contact_info['mobile_no'])){{ $contact_info['mobile_no'] }} @endif" required/>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger pull-right" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().remove()}">Remove</button>
                    </div>
                </fieldset>
            @endforeach

        @else
            <fieldset class="single-contact-info">
                    <div class="form-group">
                        <label>Contact Title</label>
                        <input type="text" class="form-control" name="contents[contact_info][0][title]" required/>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" name="contents[contact_info][0][address]" rows="8" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Phone Numbers</label>
                        <input type="text" class="form-control" name="contents[contact_info][0][phone_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" required/>
                    </div>
                    <div class="form-group">
                        <label>Mobile Numbers</label>
                        <input type="text" class="form-control" name="contents[contact_info][0][mobile_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" required/>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-danger pull-right" onclick="if(confirm('Are You Sure ?')){$(this).parent().parent().remove()}">Remove</button>
                    </div>
                </fieldset>
        @endif
    </span>

    <fieldset>
        <div class="form-group">
            <button class="btn btn-primary" id="add_contact_info" type="button">Add More +</button>
        </div>
    </fieldset>
</div>

<div class="widget-body">
    <fieldset>
        <legend>
            Location elements
        </legend>
    </fieldset>

    <fieldset>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Set your location</label>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control" placeholder="Latitude" id="current_lat" name="contents[current_lat]" value="@if(isset($page->contents['current_lat'])){{ $page->contents['current_lat'] }} @endif" data-bv-field="current lat" required>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control" placeholder="Longitude" id="current_long" name="contents[current_long]" value="@if(isset($page->contents['current_long'])){{ $page->contents['current_long'] }} @endif" data-bv-field="current long" required>
                </div>
                <div class="col-md-2">
                    <button type="button" id="getLat" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Set Location</button>
                </div>
            </div>
        </div>
    </fieldset>

    @include('admin.template.partials.form_submit')
</div>




<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {{--<h4 class="modal-title">Set Location</h4>--}}
            </div>
            <div class="modal-body">
                <div id="map" style="width: 100%; height: 450px;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>

@section('script')
    <script>
        var currentLat =23.758206;
        var currentLong =90.385208;
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition);
        }

        function showPosition(position) {
            currentLat=position.coords.latitude;
            currentLong=position.coords.longitude;
        }

        function setPosition(position) {
            document.getElementById('current_lat').value=position.lat();
            document.getElementById('current_long').value=position.lng();
        }

        function initMap() {
            var mapDiv = document.getElementById('map');
            var map = new google.maps.Map(mapDiv, {
                zoom: 12,
                center: new google.maps.LatLng(currentLat, currentLong)
            });

            var myLatlng = {lat: currentLat, lng: currentLong};

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Click to zoom',
                draggable: true,
            });

            // Zoom to 9 when clicking on marker
            google.maps.event.addListener(marker,'click',function() {
                map.setZoom(9);
                map.setCenter(marker.getPosition());
            });

            google.maps.event.addListener(map, 'click', function(event) {
                marker.setPosition(event.latLng);
                setPosition(event.latLng);
            });

            google.maps.event.addListener(map, 'drag', function() {
                marker.setPosition(map.getCenter());
            } );

            google.maps.event.addListener(map, 'dragend', function() {
                setPosition(map.getCenter());
            } );

            marker.addListener('dragend', function (event) {
                setPosition(event.latLng);
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNrASOuKNnXr4a5rEqDC_8fo_isduYSeU&callback=initMap&sensor=true">
    </script>
    <script>

        $(document).ready(function(){
            $('#myMapModal').on('shown.bs.modal', function(e) {
                var element = $(e.relatedTarget);
                var data = element.data("lat").split(',');
                initialize(new google.maps.LatLng(data[0], data[1]));
                google.maps.event.trigger(map, 'resize');
            });

            var contact_info_length=$('.single-contact-info').length;
            //    Add Form Elements
            $('#add_contact_info').click(function () {
                var data='<fieldset class="single-contact-info">\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Contact Title</label>\n' +
                    '                        <input type="text" class="form-control" name="contents[contact_info]['+contact_info_length+'][title]" required/>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Address</label>\n' +
                    '                        <textarea class="form-control" name="contents[contact_info]['+contact_info_length+'][address]" rows="8" required></textarea>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Phone Numbers</label>\n' +
                    '                        <input type="text" class="form-control" name="contents[contact_info]['+contact_info_length+'][phone_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" required/>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <label>Mobile Numbers</label>\n' +
                    '                        <input type="text" class="form-control" name="contents[contact_info]['+contact_info_length+'][mobile_no]" placeholder="XX-XXXXX, YY-YYYYY, ZZ-ZZZZZ" required/>\n' +
                    '                    </div>\n' +
                    '                    <div class="form-group">\n' +
                    '                        <button class="btn btn-danger pull-right" onclick="if(confirm(\'Are You Sure ?\')){$(this).parent().parent().remove()}">Remove</button>\n' +
                    '                    </div>\n' +
                    '                </fieldset>';
                $('#contact-info-group').append(data);

                contact_info_length++;
            });
        });

    </script>
@endsection




