<footer class="sa-page-footer">
    <div class="d-flex align-items-center w-100 h-100">
        <div class="footer-left">
            @copyright reserved by- <span class="footer-txt">GOSB </span> &copy; {{ date('Y') }}
        </div>
        <div class="ml-auto footer-right">
            <i class="hidden-xs text-blue-light">Last account activity <i class="fa fa-clock-o"></i> <strong>52 mins ago &nbsp;</strong> </i>
        </div>
    </div>
</footer>
