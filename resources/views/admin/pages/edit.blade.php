@extends('admin.layout.app')

@section('page_title','Admin | Edit page')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\PageController@index') }}">Pages</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Edit page</span></h1>
            </div>
        </div>


        <div class="w-100">
            @include('flash::message')
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" action="{{ action('Admin\PageController@update', $page->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Edit Page </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>

                                    <!-- widget content -->
                                    <span id="load-template">
                                        @include("admin.template.{$page->template['template_name']}",['page_action'=>'edit'])
                                    </span>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-search"></i> Page setting</h5><hr>
                                <div class="form-group">
                                    <label>Page title</label>
                                    <input class="form-control" placeholder="Page title" value="{{ $page->page_title }}" type="text" name="page_title" required>
                                </div>
                                <div class="form-group">
                                    <label>Page slug</label>
                                    <input class="form-control" placeholder="Page slug" value="{{ $page->slug }}" type="text" name="slug" required>
                                </div>
                                <div class="form-group">
                                    <label>Page template</label>
                                    <select class="form-control" id="page-template" name="template_id" required>
                                        @foreach($page_templates as $page_template)
                                            <option value="{{ $page_template->id }}" @if($page->template_id == $page_template->id){{ 'selected' }} @endif>{{ $page_template->template_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /input-group -->
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('div.alert').delay(3000).fadeOut(350);

            var editor_config = {
                path_absolute : "/",
                selector: "textarea.editor",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                templates: [
                    {title: 'Right Side Image With Title', description: 'Section With Right Side Image (With Title)', url: '/template/right_side_image_with_title.html'},
                    {title: 'Right Side Image Without Title', description: 'Section With Right Side Image (Without Title)', url: '/template/right_side_image_without_title.html'},
                    {title: 'Left Side Image With Title', description: 'Section With Left Side Image (With Title)', url: '/template/left_side_image_with_title.html'},
                    {title: 'Left Side Image Without Title', description: 'Section With Left Side Image (Without Title)', url: '/template/left_side_image_without_title.html'},
                    {title: 'Text With Title', description: 'Section With Text (With Title)', url: '/template/text_with_title.html'},
                    {title: 'Video With Title', description: 'Section With Video (With Title)', url: '/template/video_with_title.html'},
                ],
                init_instance_callback: function (editor) {
                    editor.on('SetContent', function (e) {
                        // editor.focus();
                        // editor.selection.select(editor.getBody(), true);
                        // editor.selection.collapse(false);
                        tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
                        tinyMCE.activeEditor.selection.collapse(false);
                        console.log(editor);
                    });
                },
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });

                    tinymce.activeEditor.dom.setStyles(tinymce.activeEditor.dom.select('section'), {'display': 'block'});
                }
            };

            function applyMCE() {
                tinymce.init(editor_config);
            }

            function AddRemoveTinyMce(editorId) {
                if(tinyMCE.get(editorId))
                {
                    tinyMCE.EditorManager.execCommand('mceFocus', false, editorId);
                    tinyMCE.EditorManager.execCommand('mceRemoveEditor', true, editorId);

                } else {
                    tinymce.EditorManager.execCommand('mceAddEditor', false, editorId);
                }
            }

            applyMCE();

            $('#page-template').change(function () {
                var page_template=$('#page-template').val();

                $.ajax({
                    type:'POST',
                    url:'/admin/load_template',
                    data:{type: 'edit', template_id: page_template, _token: '{{ csrf_token() }}', page_id: '{{ $page->id }}'},
                    success:function (response) {
                        if (response !== false){
                            $('#load-template').html(response);
                            applyMCE();
                        }
                        else {
                            alert('Something went wrong. Please try again later.');
                        }
                    }
                });

            });

            $('.colorpicker').colorpicker();

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    title : {
                        validators : {
                            notEmpty : {
                                message : 'Slider title is required'
                            },
                        }
                    },
                    subtitle : {
                        validators : {
                            notEmpty : {
                                message : 'Slider subtitle is required'
                            }
                        }
                    }
                }
            });

            // end profile form

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
