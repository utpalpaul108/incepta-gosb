@extends('admin.layout.app')

@section('page_title','Admin | Create page')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\PageController@create') }}">Create page</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Create page</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" action="{{ action('Admin\PageController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Crete Page </h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>
                                <span id="load-template">
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Form Elements
                                            </legend>
                                        </fieldset>

                                            <fieldset>
                                                <div class="form-group">
                                                    <label>Slider Title</label>
                                                    <input type="text" class="form-control" placeholder="Slider title" name="contents[slider_title]" required/>
                                                </div>
                                            </fieldset>
{{--                                            <fieldset>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Slider Subtitlet</label>--}}
{{--                                                    <textarea class="form-control" name="contents[slider_subtitle]" rows="8" placeholder="Slider subtitle"></textarea>--}}
{{--                                                </div>--}}
{{--                                            </fieldset>--}}
                                            <fieldset>
                                                <div class="form-group">
                                                    <label>Slider Image</label>
                                                    <div class="box-body text-center">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 200px;">
                                                                <img src="http://placehold.it/200x200" width="100%" alt="slider image">
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                                            <div>
                                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                                    <input type="file" name="slider_image" required>
                                                                </span>
                                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="form-group">
                                                    <label>Page content</label>
                                                    <textarea class="form-control editor" name="contents[page_content]" rows="8"></textarea>
                                                </div>
                                            </fieldset>


                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Create
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-3">
                            <!-- /well -->
                            <div class="well padding-10">
                                <h5 class="mt-0"><i class="fa fa-search"></i> Page setting</h5><hr>
                                <div class="form-group">
                                    <label>Page title</label>
                                    <input class="form-control" placeholder="Page title" type="text" name="page_title" required>
                                </div>
                                <div class="form-group">
                                    <label>Page slug</label>
                                    <input class="form-control" placeholder="Page slug" type="text" name="slug" required>
                                </div>
                                <div class="form-group">
                                    <label>Page template</label>
                                    <select class="form-control" id="page-template" name="template_id" required>
                                        @foreach($page_templates as $page_template)
                                            <option value="{{ $page_template->id }}">{{ $page_template->template_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /input-group -->
                            </div>
                            <!-- /well -->
                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            var editor_config = {
                path_absolute : "/",
                selector: "textarea.editor",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                templates: [
                    {title: 'Right Side Image With Title', description: 'Section With Right Side Image (With Title)', url: '/template/right_side_image_with_title.html'},
                    {title: 'Right Side Image Without Title', description: 'Section With Right Side Image (Without Title)', url: '/template/right_side_image_without_title.html'},
                    {title: 'Left Side Image With Title', description: 'Section With Left Side Image (With Title)', url: '/template/left_side_image_with_title.html'},
                    {title: 'Left Side Image Without Title', description: 'Section With Left Side Image (Without Title)', url: '/template/left_side_image_without_title.html'},
                    {title: 'Text With Title', description: 'Section With Text (With Title)', url: '/template/text_with_title.html'},
                    {title: 'Video With Title', description: 'Section With Video (With Title)', url: '/template/video_with_title.html'},
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };

            function applyMCE() {
                tinymce.init(editor_config);
            }

            applyMCE();

            $('#page-template').change(function () {
                var page_template=$('#page-template').val();

                $.ajax({
                    type:'POST',
                    url:'/admin/load_template',
                    data:{type: 'create', template_id: page_template, _token: '{{ csrf_token() }}'},
                    success:function (response) {
                        if (response !== false){
                            $('#load-template').html(response);
                            applyMCE();
                        }
                        else {
                            alert('Something went wrong. Please try again later.');
                        }
                    }
                });


            });

            $('.colorpicker').colorpicker();

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    title : {
                        validators : {
                            notEmpty : {
                                message : 'Slider title is required'
                            },
                        }
                    },
                    subtitle : {
                        validators : {
                            notEmpty : {
                                message : 'Slider subtitle is required'
                            }
                        }
                    }
                }
            });

            // end profile form

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
