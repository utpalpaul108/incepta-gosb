@extends('layout.app')

@section('page_title','| Specialist Details')

@section('contents')

    <section id="consultant-breadcrum">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>DOCTOR & CONSULTANT DETAILS</h2>
                    <strong><a href="/">HOME</a> // DOCTOR & CONSULTANT DETAILS</strong>
                </div>
            </div>
        </div>
    </section>


    <section id="consultant"><!-- Chairmen Message Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 fiw">
                    <img src="/storage/{{ $doctor->profile_image }}" class="img-fluid">
                    <div class="cons-name">
                        <h4>{{ $doctor->name }}</h4>
                        <span>{{ $doctor->specialist }}</span>
                        <div class="social-icons-footer">
                            <ul>
                                @if(isset($doctor->profile['social_links']['facebook']) && !is_null($doctor->profile['social_links']['facebook']))
                                    <li><a href="{{ $doctor->profile['social_links']['facebook'] }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                @endif
                                @if(isset($doctor->profile['social_links']['instagram']) && !is_null($doctor->profile['social_links']['instagram']))
                                    <li><a href="{{ $doctor->profile['social_links']['instagram'] }}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                @endif
                                @if(isset($doctor->profile['social_links']['youtube']) && !is_null($doctor->profile['social_links']['youtube']))
                                    <li><a href="{{ $doctor->profile['social_links']['youtube'] }}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                @endif
                                @if(isset($doctor->profile['social_links']['twitter']) && !is_null($doctor->profile['social_links']['twitter']))
                                    <li><a href="{{ $doctor->profile['social_links']['twitter'] }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="cons-pera">
                        <h4>{{ $doctor->name ?? '' }}</h4>
                        <h6>{{ $doctor->designation ?? '' }}</h6>
                        <p>{{ $doctor->about ?? '' }}</p>

                        <br>
                        <h6>SPECIALITY</h6>
                        <p>{{ $doctor->specialist }}</p>

                        <br>
                        <h6>EDUCATION</h6>
                        <p>{{ $doctor->profile['education'] ?? '' }}</p>
                        <br>
                        <h6>EXPERIENCE</h6>
                        <p>{{ $doctor->profile['experience'] ?? '' }}</p>
                        <br>

                        <h6>AWARDS & RECOGNITION</h6>
                        <p>{{ $doctor->profile['awards'] ?? '' }}</p>
                        <br>

                        <h6>ADDRESS</h6>
                        <p>{{ $doctor->profile['address'] ?? '' }}</p>

                        <br>
                        <h6>PHONE</h6>
                        <p>{{ $doctor->phone_no ?? '' }}</p>
                        <br>

                        <h6>WEBSITE</h6>
                        <p>{{ $doctor->profile['website'] ?? '' }}</p>
                    </div>

                </div>
            </div>
        </div>

{{--        <div id="related-doctors"><!--  Related Doctors Area Start -->--}}
{{--            <div class="container">--}}
{{--                <div class="doc-heading">--}}
{{--                    <h3><b>RELATED DOCTORS</b></h3>--}}
{{--                    <img src="/images/dash.png"/>--}}
{{--                </div>--}}
{{--                <div class="row fiw">--}}
{{--                    <div class="col-md-3">--}}
{{--                        <a href="#"><img src="/images/doc-1.jpg"/></a>--}}
{{--                        <div class="box-detail">--}}
{{--                            <h5><a class="dark" href="#">DR. Andrew Wells</a></h5>--}}
{{--                            <span>Surgeon</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3">--}}
{{--                        <a href="#"><img src="/images/doc-2.jpg"/></a>--}}
{{--                        <div class="box-detail">--}}
{{--                            <h5><a class="dark" href="#">DR. Amy Murray</a></h5>--}}
{{--                            <span>Gyaneologist</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3">--}}
{{--                        <a href="#"><img src="/images/doc-3.jpg"/></a>--}}
{{--                        <div class="box-detail">--}}
{{--                            <h5><a class="dark" href="#">DR. Anthony Bowman</a></h5>--}}
{{--                            <span>Surgeon</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3">--}}
{{--                        <a href="#"><img src="/images/doc-4.jpg"/></a>--}}
{{--                        <div class="box-detail">--}}
{{--                            <h5><a class="dark" href="#">DR. Katherine Turner</a></h5>--}}
{{--                            <span>Gyaneologist</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div><!-- Related Doctors Area End -->--}}

    </section>

@endsection
