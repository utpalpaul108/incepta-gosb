@extends('layout.app')

@section('contents')
    @include('partials.slider')
    {!! $page->contents['page_content'] ?? '' !!}
@endsection
