@extends('layout.app')

@section('page_title','| Conference Details')

@section('contents')

    <section id="conference-breadcrum">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>CONFERENCE DETAILS</h2>
                    <strong><a href="#">HOME</a> // CONFERENCE DETAILS</strong>
                </div>
            </div>
        </div>
    </section>

    <section id="conference">
        <div class="container">
            <h4 style="text-transform: uppercase"><b>{{ $conference->title }}</b></h4>
            <div class="row ptd">
                <div class="col-md-12">
                    {!! $conference->details !!}
                </div>
            </div>
        </div>
    </section>

@endsection
