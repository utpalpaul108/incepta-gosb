
<!DOCTYPE html>
<html>
<head>
    <!--Head Satrted-->
    <title>GOSB  @yield('page_title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viweport" content="width=device-width ,initial-scale=1">
    <meta name="author" content="" />
    <meta content="" name="description" />
    <meta content="" name="keywords" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />


    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/css/slick-theme.css">
    @yield('style')
</head>
<!-- Head Ended -->
<body>
<!-- Body Area Started -->

@include('partials.header')

@yield('contents')

@include('partials.footer')

{{-- For Login --}}

@include('partials.login')

{{-- For Register --}}

@include('partials.register')

@include('partials.session_message')

<script src="/js/jquery-3.3.1.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/slick.min.js"></script>

<!-- Register Start -->

<script>
    // Get the modal
    var modal = document.getElementById('reg');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

<!-- Register end -->
<script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
    $(document).ready(function(){
        $('.banner-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            arrows: true
        });

        var success_message="{{ session('success_message') }}";
        var warning_message="{{ session('warning_message') }}";

        if(success_message != ''){
            // $('#myModal').modal('show');
            document.getElementById('success_message').style.display='block';
        };

        if(warning_message != ''){
            // $('#myModal').modal('show');
            document.getElementById('warning_message').style.display='block';
        };
        $('.registar').on('click',function (e) {
            e.preventDefault();
            document.getElementById('id01').style.display='none';
            document.getElementById('reg').style.display='block'
        });

        $('.login-user').on('click',function (e) {
            e.preventDefault();
            document.getElementById('reg').style.display='none';
            document.getElementById('id01').style.display='block'
        });


    });
</script>
<script>
    $(document).ready(function(){
        $(".search-toggler").click(function(){
            $( "#mobileSearch" ).toggleClass( "active-ms" );

        });


    });
</script>
@yield('script')
<!-- Sticky -->
<!-- For Slider End -->
</body><!-- Body Area Ended -->
</html>
