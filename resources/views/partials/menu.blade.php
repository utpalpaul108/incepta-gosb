@inject('request', 'Illuminate\Http\Request')

<ul class="navbar-nav ml-auto">
    @foreach($items as $item)
        <li class="nav-item {{ request()->is($item->url) ? 'join' : '' }} {{ !$item->children->isEmpty() ? 'dropdown-toggle' : '' }}" >
            <a class="nav-link" href="/{{ $item->url }}">{{ strtoupper($item->title) }} @if(!$item->children->isEmpty())<b class="caret"></b> @else <span class="sr-only">(current)</span> @endif</a>
            @if(!$item->children->isEmpty())
                @include('partials.menu-items', ['items' => $item->children])
            @endif
        </li>
    @endforeach

    <li class="nav-item ">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
            @if(Auth::check())
                {{ strtoupper(Auth::user()->name ) }}
            @else
                JOIN WITH US
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            @if(Auth::check())
                <li class="nav-item">
                    <a href="{{ action('UserController@edit_profile') }}" class="nav-link">Edit Profile</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('UserController@user_logout') }}" class="nav-link">Logout</a>
                </li>
            @else
                <li class="nav-item">
                    <a href="" class="nav-link registar">Register</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link login-user">Login</a>
                </li>
            @endif
        </ul>
    </li>
</ul>

