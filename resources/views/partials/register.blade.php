<div id="reg" class="modal">

    <form class="modal-content animate" action="{{ action('UserController@store') }}" method="post">
        @csrf
        <div class="imgcontainer">
            <span onclick="document.getElementById('reg').style.display='none'" class="close" title="Close Modal">&times;</span>
            <img src="/images/logo.png" alt="Avatar" style="width: 100px" height="100px">
            <h3>REGISTER AS</h3>
        </div>

        <div class="container">
            <div class="">
                <label class="radio-inline after-1">
                    <input type="radio" name="user_type" value="doctor" checked>DOCTOR
                </label>
                <label class="radio-inline after-2">
                    <input type="radio" name="user_type" value="consultant">CONSULTANT
                </label>
                <label class="radio-inline after-3">
                    <input type="radio" name="user_type" value="user">USER
                </label>
            </div>
            <input type="text" placeholder="Full Name" name="name" required>
            <label class="radio-inline after-1">
                <input type="radio" name="gender" value="male" checked>MALE
            </label>
            <label class="radio-inline after-2">
                <input type="radio" name="gender" value="female">FEMALE
            </label>
            <input type="text" placeholder="Specialist in" name="specialist" required>
            <input type="text" placeholder="Email Address" name="email" required>
            <input type="text" placeholder="Phone Number" name="phone_no" required>
            <input type="password" placeholder="Password" name="password" required>

            <button type="submit" class="btn btn-default login-btn" >Register Now</button>
        </div>

        <div class="container pd">
            <p style="text-align: center;">Already Have an Account ? <a href="#" class="login-user">Login</a></p>
        </div>
    </form>
</div>
