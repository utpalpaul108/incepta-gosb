
<div class="desktop-header">

    <section id="up-header"><!-- Up Header area started -->
        <div class="container"><!-- Container -->
            <div class="row"><!-- Main Row -->
                <div class="col-md-3 col-5"><!-- Col = 3  -->
                    <div class="logo">
                        <a href="/">
                            <img src="{{ '/storage/' .setting('logo') ?? '' }}" alt="logo" class="img-fluid"/>
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-7 " id="dd"><!-- Col = 9  -->
                    <div class="row"><!-- Inner Row 1-->
                        <div class="col-md-5"><!-- Col = 5  -->
                            <div class="social-icons">
                                @if(!is_null(setting('social_icons')))
                                    <ul>
                                        @foreach(json_decode(setting('social_icons'),true) as $social_icon)
                                            <li>
                                                <a href="{{ $social_icon['url'] ?? '' }}">
                                                    <i class="fab {{ $social_icon['icon'] }}"></i>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4"><!-- Col = 4  -->
                            <div class="uh-email">
                                <i class="fas fa-envelope"></i> <a href = "{{ setting('email') ?? '' }}"  target="_top">{{ setting('email') ?? '' }}</a>
                            </div>
                        </div>
                        <div class="col-md-3"><!-- Col = 3  -->
                            <div class="uh-phone">
                                <i class="fas fa-phone-volume"></i><a href="tel:{{ setting('phone_no') ?? '' }}">{{ setting('phone_no') ?? '' }}</a>
                            </div>
                        </div>
                    </div><!-- Inner Row 1-->
                    <div class="row"><!-- Inner Row 2-->
                        <div class="col-md-9 col-sm-6"><!-- Col = 9  -->
                            <div class="up-search d-none d-sm-none d-md-block">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search Here You Want To Know">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6"><!-- Col = 3  -->
                            <div class="uh-button d-none d-sm-none d-md-block">
                                <button onclick="document.getElementById('id01').style.display='block'" class="btn btn-default">DONATE NOW</button>
                            </div>
                        </div>
                    </div><!-- Inner Row 2-->
                </div>
            </div><!-- Main Row -->
        </div><!-- Container -->
    </section><!-- Up Header area Ended -->

    <header id="navbar"><!-- Starting Header Area -->
        <!-- Nav-bar Starting area -->
        <div class="container fw"><!-- Container Area Starting -->
            <div class="nav">

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        @include('partials.menu')
                    </div>


                </nav>
                <button class="search-toggler align-right" type="button" data-target="#mobileSearch">
                    <i class="fas fa-search"></i>
                </button>

{{--                <a href="#" class="donate-now align-right join" onclick="document.getElementById('id01').style.display='block'">--}}
{{--                    Donate Now--}}
{{--                </a>--}}


            </div>
        </div><!-- Container area Ending -->
        <!-- Nav-bar Ending area -->
    </header><!-- Ending Header Area  -->
</div>

<div id="mobileSearch">

    <div class="col-md-12"><!-- Col = 9  -->
        <div class="m-search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search Here You Want To Know">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>
