
<footer id="footer"><!-- Footer Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="gobs">
                    <h4><b>{{ $footer->contents['section_one']['title'] ?? '' }}</b></h4>
                    <p>{{ $footer->contents['section_one']['details'] ?? '' }}</p>
                    <p><span>Phone : </span> {{ setting('phone_no') ?? '' }}</p>
                    <p><span>Email : </span> {{ setting('email') ?? '' }}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="quick-link">
                    <h4><b>{{ $footer->contents['section_two']['title'] ?? '' }}</b></h4>
                    @if(isset($footer->contents['section_two']['quick_links']) && !is_null($footer->contents['section_two']['quick_links']))
                        <ul>
                            @foreach($footer->contents['section_two']['quick_links'] as $quick_link)
                                <li><a href="{{ $quick_link['url'] ?? '' }}">{{ $quick_link['name'] ?? '' }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="subscribe-us">
                    <h4><b>{{ $footer->contents['section_three']['title'] ?? '' }}</b></h4>
                    <p>{{ $footer->contents['section_three']['details'] ?? '' }}</p>
                    <div class="main">
                        <!-- Another variation with a button -->
                        <form action="{{ action('SubscriberController@store') }}" method="post">
                            @csrf
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" placeholder="Email Address" required>
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fas fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="social-icons-footer">
                        @if(!is_null(setting('social_icons')))
                            <ul>
                                @foreach(json_decode(setting('social_icons'),true) as $social_icon)
                                    <li>
                                        <a href="{{ $social_icon['url'] ?? '' }}">
                                            <i class="fab {{ $social_icon['icon'] }}"></i>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- Footer end -->
<section id="copywrite"><!-- Copywrite start -->
    <p>Copyright © {{ date('Y') }} <span>GOSB.</span> All Rights Reserved.<br>
        Design & Developed By ITCLANBD.</p>
</section><!-- Copywrite end  -->
