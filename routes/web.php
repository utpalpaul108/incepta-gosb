<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@show');
Route::get('/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout');
Route::post('/user-authenticate', 'UserController@user_authenticate');
Route::get('/user-logout', 'UserController@user_logout');
Route::post('/authenticate', 'UserController@authenticate');
Route::post('/subscribe', 'SubscriberController@store');
Route::post('/register', 'UserController@store');
Route::post('/user-authenticate', 'UserController@user_authenticate');
Route::get('/edit-profile', 'UserController@edit_profile')->middleware('auth');
Route::post('/update-profile', 'UserController@update_profile')->middleware('auth');
Route::get('/user-logout', 'UserController@user_logout')->middleware('auth');
Route::get('/doctor-details/{id}', 'DoctorController@details');
Route::get('/conference-details/{id}', 'ConferenceController@details');

// For Admin Panel
// ===============================

Route::group(['prefix'=>'/admin', 'middleware'=>'isAdmin', 'namespace'=>'Admin'],function (){

    Route::get('/', 'DashboardController@index');

//    For Dynamic Menu
//    ==================================

    Route::get('/menu', 'MenuController@index');
    Route::post('/menu/create', 'MenuItemController@store');
    Route::get('/menu/delete/{id}', 'MenuItemController@destroy');
    Route::get('/menu/load_data', 'MenuController@loadItems');
    Route::post('/menu/update', 'MenuItemController@update');
    Route::post('/menu/order', 'MenuController@order_item')->name('menus.order');

//    For Dynamic Slider
//    =====================================
    Route::resource('/slider-groups', 'SliderGroupController');
    Route::resource('/sliders', 'SliderController');

//    For Subscribers
//    =====================================
    Route::resource('/subscribers', 'SubscriberController');

//    For Categories
//    =====================================
    Route::resource('/categories', 'CategoryController');
    Route::resource('/gallery-categories', 'GalleryCategoryController');

//    For Blog Posts
//    =====================================
    Route::resource('/blog-posts', 'BlogPostController');

//    For Gallery
//    =====================================
    Route::resource('/galleries', 'GalleryController');

//    For Dynamic Page
//    =====================================
    Route::post('/load_template','PageController@load_template');
    Route::resource('/pages', 'PageController');

//    For Site Settings
//    =====================================
    Route::get('/site-settings', 'SiteSettingController@index');
    Route::post('/site-settings', 'SiteSettingController@update');

//    For Conference
//    =====================================
    Route::resource('/conference-types', 'ConferenceTypeController');
    Route::resource('/conferences', 'ConferenceController');

//    For Forum Posts
//    =====================================
    Route::resource('/forums', 'ForumController');

//    For Widget
//    =====================================
    Route::resource('/widgets', 'WidgetController');

//    For Events
//    =====================================
    Route::resource('/events', 'EventController');

//    For Memberships
//    =====================================
    Route::get('/remove-membership-document/{id}','MembershipController@remove_image');
    Route::resource('/memberships', 'MembershipController');

//    For Pending Users
    Route::get('/approve-user/{id}', 'PendingUserController@approve_user');
    Route::resource('/pending-users', 'PendingUserController');

//    For Doctor & Consultant
    Route::resource('/doctor-consultants', 'DoctorController');


});

// For Viw Pages
// ===========================

Route::get('/{slug}','PageController@show')->name('page.show');
